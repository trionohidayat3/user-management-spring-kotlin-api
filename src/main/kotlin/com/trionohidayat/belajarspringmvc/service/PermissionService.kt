package com.trionohidayat.belajarspringmvc.service

import com.trionohidayat.belajarspringmvc.repository.PermissionRepository
import com.trionohidayat.belajarspringmvc.entity.Permission
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class PermissionService(@Autowired private val permissionRepository: PermissionRepository) {

    @Transactional(readOnly = true)
    fun getAllPermissions(): List<Permission> {
        return permissionRepository.findAll()
    }

    @Transactional(readOnly = true)
    fun getPermissionById(id: Long): Permission? {
        return permissionRepository.findById(id).orElse(null)
    }

    @Transactional
    fun createPermission(permission: Permission): Permission {
        return permissionRepository.save(permission)
    }

    @Transactional
    fun updatePermission(id: Long, updatedPermission: Permission): Permission? {
        val existingPermission = permissionRepository.findById(id)
        if (existingPermission.isPresent) {
            val permission = existingPermission.get()
            permission.name = updatedPermission.name
            return permissionRepository.save(permission)
        }
        return null
    }

    @Transactional
    fun deletePermission(id: Long) {
        permissionRepository.deleteById(id)
    }

    @Transactional
    fun getPermissionsByIds(permissionIds: Set<Long>): List<Permission> {
        return permissionRepository.findAllById(permissionIds)
    }
}
