package com.trionohidayat.belajarspringmvc.service

import com.trionohidayat.belajarspringmvc.repository.RoleRepository
import com.trionohidayat.belajarspringmvc.entity.Role
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class RoleService(@Autowired private val roleRepository: RoleRepository) {

    @Transactional(readOnly = true)
    fun getAllRoles(): List<Role> {
        return roleRepository.findAll()
    }

    @Transactional(readOnly = true)
    fun getRoleById(id: Long): Role? {
        return roleRepository.findById(id).orElse(null)
    }

    @Transactional
    fun createRole(role: Role): Role {
        return roleRepository.save(role)
    }

    @Transactional
    fun updateRole(id: Long, updatedRole: Role): Role? {
        val existingRole = roleRepository.findById(id)
        if (existingRole.isPresent) {
            val role = existingRole.get()
            role.name = updatedRole.name
            role.permissions = updatedRole.permissions
            return roleRepository.save(role)
        }
        return null
    }

    @Transactional
    fun deleteRole(id: Long) {
        roleRepository.deleteById(id)
    }
}
