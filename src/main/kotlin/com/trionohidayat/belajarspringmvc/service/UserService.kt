package com.trionohidayat.belajarspringmvc.service

import com.trionohidayat.belajarspringmvc.repository.UserRepository
import com.trionohidayat.belajarspringmvc.entity.User
import jakarta.persistence.EntityNotFoundException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.time.LocalDateTime

@Service
class UserService(
    @Autowired private val userRepository: UserRepository,
    @Autowired private val roleService: RoleService
) {

    @Transactional(readOnly = true)
    fun getAllUsers(): List<User> {
        return userRepository.findAll()
    }

    @Transactional(readOnly = true)
    fun getUserById(id: Long): User? {
        return userRepository.findById(id).orElse(null)
    }

    @Transactional
    fun createUser(user: User, roleId: Long): User {
        val role = roleService.getRoleById(roleId)
        role?.let { user.role = it }

        user.createdAt = LocalDateTime.now()
        user.updatedAt = LocalDateTime.now()
        return userRepository.save(user)
    }

    @Transactional
    fun updateUser(id: Long, updatedUser: User, roleId: Long): User? {
        val existingUser = userRepository.findById(id)
        if (existingUser.isPresent) {
            val user = existingUser.get()
            user.fullname = updatedUser.fullname
            user.username = updatedUser.username
            user.password = updatedUser.password
            user.email = updatedUser.email
            user.active = updatedUser.active

            val role = roleService.getRoleById(roleId)
            role?.let { user.role = it }

            updatedUser.updatedAt = LocalDateTime.now()
            return userRepository.save(user)
        }
        return null
    }

    @Transactional
    fun updatePassword(id: Long, oldPassword: String, newPassword: String, confirmPassword: String) {
        val userOptional = userRepository.findById(id)
        if (userOptional.isPresent) {
            val user = userOptional.get()

            // Check if old password matches (customize your validation logic)
            if (oldPassword != user.password) {
                throw IllegalArgumentException("Old password is incorrect")
            }

            // Check if new password and confirm password match
            if (newPassword != confirmPassword) {
                throw IllegalArgumentException("New password and confirm password do not match")
            }

            // Update the password (customize your encryption logic)
            user.password = newPassword
            userRepository.save(user)
        } else {
            throw EntityNotFoundException("User with ID $id not found")
        }
    }



    @Transactional
    fun deleteUser(id: Long) {
        userRepository.deleteById(id)
    }

    @Transactional(readOnly = true)
    fun getUserByEmail(email: String): User? {
        return userRepository.findByEmail(email)
    }
}
