package com.trionohidayat.belajarspringmvc.entity

import jakarta.persistence.*
import java.time.LocalDateTime

@Entity
@Table(name = "tbl_role")
data class Role(
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long? = null,

    var name: String,

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
        name = "tbl_role_permissions",
        joinColumns = [JoinColumn(name = "role_id")],
        inverseJoinColumns = [JoinColumn(name = "permission_id")]
    )
    var permissions: Set<Permission> = emptySet(),

    @Column(nullable = false)
    var createdAt: LocalDateTime = LocalDateTime.now(),

    @Column(nullable = false)
    var updatedAt: LocalDateTime = LocalDateTime.now()

)
