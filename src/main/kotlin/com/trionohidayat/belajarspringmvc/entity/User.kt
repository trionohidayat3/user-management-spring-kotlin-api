package com.trionohidayat.belajarspringmvc.entity

import jakarta.persistence.*
import java.time.LocalDateTime

@Entity
@Table(name = "tbl_user")
data class User(
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long? = null,

    var fullname: String,
    var username: String,
    var password: String,
    var email: String,
    var active: Boolean,

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "role_id")
    var role: Role? = null,

    @Column(nullable = false)
    var createdAt: LocalDateTime = LocalDateTime.now(),

    @Column(nullable = false)
    var updatedAt: LocalDateTime = LocalDateTime.now()
)
