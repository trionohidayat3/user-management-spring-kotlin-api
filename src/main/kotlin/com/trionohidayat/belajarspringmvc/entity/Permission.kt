package com.trionohidayat.belajarspringmvc.entity

import jakarta.persistence.*
import java.time.LocalDateTime

@Entity
@Table(name = "tbl_permission")
data class Permission(
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long? = null,

    var name: String,

    @Column(nullable = false)
    var createdAt: LocalDateTime = LocalDateTime.now(),

    @Column(nullable = false)
    var updatedAt: LocalDateTime = LocalDateTime.now()
)
