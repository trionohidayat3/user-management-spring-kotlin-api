package com.trionohidayat.belajarspringmvc

import io.jsonwebtoken.Claims
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import java.util.*

@Component
class JwtUtils {

    @Value("\${jwt.secret}")
    private lateinit var secret: String

    @Value("\${jwt.expiration}")
    private var expiration: Long = 0

    fun generateToken(username: String): String {
        val expirationDate = Date(System.currentTimeMillis() + expiration)
        return Jwts.builder()
            .setSubject(username)
            .setExpiration(expirationDate)
            .signWith(SignatureAlgorithm.HS512, secret)
            .compact()
    }

    fun getUsernameFromToken(token: String): String {
        return Jwts.parser()
            .setSigningKey(secret)
            .parseClaimsJws(token)
            .body
            .subject
    }

    fun validateToken(token: String): Boolean {
        val expirationDate = Jwts.parser()
            .setSigningKey(secret)
            .parseClaimsJws(token)
            .body
            .expiration
        return !expirationDate.before(Date())
    }
}