package com.trionohidayat.belajarspringmvc.controller

import com.trionohidayat.belajarspringmvc.entity.Role
import com.trionohidayat.belajarspringmvc.service.PermissionService
import com.trionohidayat.belajarspringmvc.service.RoleService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import java.time.LocalDateTime

@RestController
@RequestMapping("/api/roles")
class RoleController(@Autowired private val roleService: RoleService,
                     @Autowired private val permissionService: PermissionService
) {

    @GetMapping
    fun getAllRoles(): List<Role> {
        return roleService.getAllRoles()
    }

    @GetMapping("/{id}")
    fun getRoleById(@PathVariable id: Long): Role? {
        return roleService.getRoleById(id)
    }

    @PostMapping
    fun createRole(@RequestParam("name") name: String,
                   @RequestParam("permissions") permissionIds: List<Long>): Role {
        val role = Role(name = name)
        val permissions = permissionService.getPermissionsByIds(permissionIds.toSet())
        role.permissions = HashSet(permissions)
        role.createdAt = LocalDateTime.now()
        role.updatedAt = LocalDateTime.now()
        return roleService.createRole(role)
    }

    @PutMapping("/{id}")
    fun updateRole(
        @PathVariable id: Long,
        @RequestParam("name") name: String
    ): Role? {
        val updatedRole = Role(id, name)
        updatedRole.updatedAt = LocalDateTime.now()
        return roleService.updateRole(id, updatedRole)
    }

    @DeleteMapping("/{id}")
    fun deleteRole(@PathVariable id: Long) {
        roleService.deleteRole(id)
    }
}
