package com.trionohidayat.belajarspringmvc.controller

import com.trionohidayat.belajarspringmvc.entity.User
import com.trionohidayat.belajarspringmvc.service.RoleService
import com.trionohidayat.belajarspringmvc.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import java.time.LocalDateTime

@RestController
@RequestMapping("/api/users")
class UserController(
    @Autowired private val userService: UserService,
    @Autowired private val roleService: RoleService
) {

    @GetMapping
    fun getAllUsers(): List<User> {
        return userService.getAllUsers()
    }

    @GetMapping("/{id}")
    fun getUserById(@PathVariable id: Long): User? {
        return userService.getUserById(id)
    }

    @PostMapping
    fun createUser(
        @RequestParam("fullname") fullname: String,
        @RequestParam("username") username: String,
        @RequestParam("password") password: String,
        @RequestParam("email") email: String,
        @RequestParam("active") active: Boolean,
        @RequestParam("roleId") roleId: Long
    ): User {
        val user = User(
            fullname = fullname,
            username = username,
            password = password,
            email = email,
            active = active,
            role = roleService.getRoleById(roleId)
        )
        return userService.createUser(user, roleId)
    }

    @PutMapping("/{id}")
    fun updateUser(
        @PathVariable id: Long,
        @RequestParam("fullname") fullname: String,
        @RequestParam("username") username: String,
        @RequestParam("password") password: String,
        @RequestParam("email") email: String,
        @RequestParam("active") active: Boolean,
        @RequestParam("roleId") roleId: Long
    ): User? {
        val updatedUser = User(id, fullname, username, password, email, active)

        return userService.updateUser(id, updatedUser, roleId)
    }

    @PutMapping("/{id}/update-password")
    fun updatePassword(
        @PathVariable id: Long,
        @RequestParam("oldPassword") oldPassword: String,
        @RequestParam("newPassword") newPassword: String,
        @RequestParam("confirmPassword") confirmPassword: String
    ) {
        userService.updatePassword(id, oldPassword, newPassword, confirmPassword)
    }

    @DeleteMapping("/{id}")
    fun deleteUser(@PathVariable id: Long) {
        userService.deleteUser(id)
    }
}
