package com.trionohidayat.belajarspringmvc.controller

import com.trionohidayat.belajarspringmvc.JwtUtils
import com.trionohidayat.belajarspringmvc.LoginResponse
import com.trionohidayat.belajarspringmvc.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/auth")
class AuthController(
    @Autowired private val userService: UserService,
    private val jwtUtils: JwtUtils // Inject JwtUtils
) {

    @PostMapping("/login")
    fun login(
        @RequestParam("email") email: String,
        @RequestParam("password") password: String
    ): ResponseEntity<LoginResponse> {
        val user = userService.getUserByEmail(email)

        return if (user != null && user.password == password) {
            // Generate JWT token
            val token = jwtUtils.generateToken(user.email)

            ResponseEntity.ok(LoginResponse("Login successful for user: ${user.fullname}", token))
        } else {
            ResponseEntity.status(401).body(LoginResponse("Invalid credentials", null))
        }
    }

    // Example of how to use JWT token in a protected endpoint
    @GetMapping("/protected")
    fun protectedEndpoint(@RequestHeader("Authorization") token: String): ResponseEntity<String> {
        // Validate the JWT token
        if (jwtUtils.validateToken(token)) {
            val username = jwtUtils.getUsernameFromToken(token)
            return ResponseEntity.ok("Access granted for user: $username")
        }
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Invalid token")
    }
}
