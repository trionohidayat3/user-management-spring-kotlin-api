package com.trionohidayat.belajarspringmvc.controller

import com.trionohidayat.belajarspringmvc.entity.Permission
import com.trionohidayat.belajarspringmvc.service.PermissionService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import java.time.LocalDateTime

@RestController
@RequestMapping("/api/permissions")
class PermissionController(@Autowired private val permissionService: PermissionService) {

    @GetMapping
    fun getAllPermissions(): List<Permission> {
        return permissionService.getAllPermissions()
    }

    @GetMapping("/{id}")
    fun getPermissionById(@PathVariable id: Long): Permission? {
        return permissionService.getPermissionById(id)
    }

    @PostMapping
    fun createPermission(@RequestParam("name") name: String): Permission {
        val permission = Permission(name = name)
        permission.createdAt = LocalDateTime.now()
        permission.updatedAt = LocalDateTime.now()
        return permissionService.createPermission(permission)
    }


    @PutMapping("/{id}")
    fun updatePermission(
        @PathVariable id: Long,
        @RequestParam("name") name: String
    ): Permission? {
        val updatedPermission = Permission(id, name)
        updatedPermission.updatedAt = LocalDateTime.now()
        return permissionService.updatePermission(id, updatedPermission)
    }


    @DeleteMapping("/{id}")
    fun deletePermission(@PathVariable id: Long) {
        permissionService.deletePermission(id)
    }
}
