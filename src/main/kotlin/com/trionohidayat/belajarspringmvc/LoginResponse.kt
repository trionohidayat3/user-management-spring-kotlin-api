package com.trionohidayat.belajarspringmvc

data class LoginResponse(val message: String, val token: String?)
