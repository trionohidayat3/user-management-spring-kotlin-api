package com.trionohidayat.belajarspringmvc.repository

import com.trionohidayat.belajarspringmvc.entity.Role
import org.springframework.data.jpa.repository.JpaRepository

interface RoleRepository : JpaRepository<Role, Long>