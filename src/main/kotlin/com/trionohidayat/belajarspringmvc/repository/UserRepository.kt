package com.trionohidayat.belajarspringmvc.repository

import com.trionohidayat.belajarspringmvc.entity.User
import org.springframework.data.jpa.repository.JpaRepository

interface UserRepository : JpaRepository<User, Long> {
    fun findByEmail(email: String): User?
}