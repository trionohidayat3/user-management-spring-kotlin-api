package com.trionohidayat.belajarspringmvc.repository

import com.trionohidayat.belajarspringmvc.entity.Permission
import org.springframework.data.jpa.repository.JpaRepository

interface PermissionRepository : JpaRepository<Permission, Long>