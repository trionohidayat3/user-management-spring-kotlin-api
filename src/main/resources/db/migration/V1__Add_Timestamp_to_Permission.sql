-- db/migration/V1__Add_Timestamp_to_User.sql

-- Add 'created_at' and 'updated_at' columns to the 'user' table
ALTER TABLE permission
ADD COLUMN created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
ADD COLUMN updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP;
